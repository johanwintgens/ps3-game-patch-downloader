# PS3 Game Patch Downloader

A simple program to help you update your PS3 games with ease.

1. Open the program
2. Copy a game's serial and press enter
3. The program will download all available patches for that game into the directory where the program is located.
4. Move them to where you want them (I prefer the game's "PS3_UPDATE' directory)
5. Do what you needed them for 😉
