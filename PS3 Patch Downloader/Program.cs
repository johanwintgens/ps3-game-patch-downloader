﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace PS3_Patch_Downloader
{
    class Program
    {
        static void Main()
        {
            try
            {
                Console.WriteLine("Please copy the PS3 game code below");
                Console.WriteLine();

                var serial = Console.ReadLine();

                ServicePointManager.ServerCertificateValidationCallback +=
                    (mender, certificate, chain, sslPolicyErrors) => true;

                DownloadPatches(serial);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        static void DownloadPatches(string serial)
        {
            var url = CreateUrl(serial);
            var content = GetContent(url);
            var urls = GetUrls(content);

            foreach (var uri in urls)
            {
                DownloadFile(uri);
            }
        }

        static void DownloadFile(string uri)
        {
            var fileName = uri.Split("/").Last();

            using var client = new WebClient();

            client.DownloadFile(uri, fileName);
        }

        static string CreateUrl(string serial)
        {
            return string.Format("https://a0.ww.np.dl.playstation.net/tpl/np/{0}/{0}-ver.xml", serial);
        }

        static string GetContent(string url)
        {
            using var client = new WebClient();

            return client.DownloadString(url);
        }

        static IEnumerable<string> GetUrls(string content)
        {
            foreach (var s in content.Split(' '))
            {
                if (s.StartsWith("url"))
                {
                    yield return s[5..^1];
                }
            }
        }
    }
}
